# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready( ->

  $headerRow = $(".lesson_header").closest(".row")
  $practiceChekcpointId = $('.practice_checkpoint_id')
  $bar = $('#progress_bar')
  $numerator = $('#numerator')
  $progressBox = $('.progress_box');

  $(window).scroll ->
    if $(window).scrollTop() > $headerRow.height()+$headerRow.offset()['top']
      $progressBox.css('position','fixed');
    else
      $progressBox.css('position','relative');
    return

  calcProgress = ->
    count = 0;
    checked = 0;
    $practiceChekcpointId.each(->
      $this = $(this)
      count++
      if $this.prop('checked')
        checked++
      return
    )
    $numerator.text(checked+'/'+count)
    parcentage = String(Math.round(checked/count*100))+'%'
    $bar.css("width", parcentage)
    $bar.text(parcentage)
    return

  $practiceChekcpointId.on('change', (e)->
    $this = $(this)
    val = $this.val()
    $form = $this.closest('form')
    data = $form.serializeArray()
    if val > 0 && $this.prop("checked")
      $.ajax(
        $form.attr('action'),
        {
          data:data,
          method:'post'
        }
      ).done(->
        calcProgress()
        return
      )
      return
  )

  calcProgress()
  return
)
