class LessonsController < ApplicationController
  before_action :authenticate_user!
  def index
    @lessons = Lesson.order(:number).includes(:programs).all()
  end

  def show
    practices = Practice.where(user_id: current_user.id).map do |practice|
      [practice.checkpoint_id, practice]
    end
    @checked = Hash[*practices.flatten]
    @practice = Practice.new
    num = params[:id]
    render(num)
  end
end
