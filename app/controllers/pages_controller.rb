class PagesController < ApplicationController
  before_action :authenticate_user!
  def index
    @lessons = Lesson.order(:number).includes(:programs).all()
  end
end
