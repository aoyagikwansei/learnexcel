class PracticesController < ApplicationController
  before_action :authenticate_user!
  def create
    practice = Practice.new(practice_params)
    practice.user_id = current_user.id
    practice.save
    render json: practice
  end

  def practice_params
    params.require(:practice).permit(:checkpoint_id)
  end
end
