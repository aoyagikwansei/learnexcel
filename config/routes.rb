Rails.application.routes.draw do
  devise_for :users
  get 'pages', to: 'pages#index'
  root to: 'pages#index'
  resources :lessons, only: [:show, :index]
  resources :practices, only: [:create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
