class CreatePractices < ActiveRecord::Migration[5.0]
  def change
    create_table :practices do |t|


      t.references :user, index: true
      t.references :checkpoint, index: true

      t.timestamps
    end
  end
end
