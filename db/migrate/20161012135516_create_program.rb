class CreateProgram < ActiveRecord::Migration[5.0]
  def change
    create_table :programs do |t|
      t.references :lesson, foreign_key: true
      t.integer :number
      t.string :name
    end
  end
end
