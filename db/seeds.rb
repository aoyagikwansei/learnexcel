# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# user = User.new(:email => 'admin@test.com', :password => 'testtest')
# user.save!

Lesson.create!([
                   {name: "はじめに", number: 1},
                   {name: "ショートカットを使い慣れていこう", number: 2}
               ])
Program.create!([
                    {lesson_id: 1, number: 1, name: "エクセルを起動し、新規作成してみよう"},
                    {lesson_id: 1, number: 2, name: "エクセルファイルを保存してみよう。"},
                    {lesson_id: 1, number: 3, name: "隣のシートを作成し、シート間移動する。"},
                    {lesson_id: 1, number: 4, name: "実際にシートにデータを記入していこう"},
                    {lesson_id: 1, number: 5, name: "簡単な関数を使ってみよう"},
                    {lesson_id: 2, number: 1, name: "形式を選択して貼り付けてみよう"},
                    {lesson_id: 2, number: 2, name: "値を指定してコピーしてみよう"}
                ])
Checkpoint.create!([
                       {name: nil, lesson_id: 1}
                   ])